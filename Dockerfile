FROM geerlingguy/docker-debian10-ansible:latest
COPY sources.list /etc/apt/sources.list
RUN apt-get update && apt-get upgrade -y && apt-get install -y curl wget apt-transport-https dirmngr python3-apt
